Now this is completely useless, it is more of a dummy project, as it was part
of our Python course in organisation. It's weakest point is random selection of
two lists of characters.

## How could I possibly make it better?

Well, first I could try and ditch the part where it selects sets by using
`prelim_list = randint(1,2)`. 

* I could make a function that would check the prev. characters and be sure that 
it is not repeating itself.

* I could make a function that would generate sets of characters and special
characters, and then create other function which will for an argument take
those previously mentioned sets, and then randomly select one char from each.
In the end this sounds like the okay-ish solution to me. Will give it a shot in a
few weeks.
