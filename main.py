from random import *
import string
import random


def randomWordLen():
    len_nmbr = randint(15, 25)
    return len_nmbr


def randomChar(input_set_spec_char, input_set_letters, lenght):
    prelim_pass = []
    for i in range(0, lenght):
        prelim_list = randint(1, 2)
        if prelim_list == 1:
            prelim_pass.append(random.choice(input_set_letters))
        else:
            prelim_pass.append(random.choice(input_set_spec_char))
    return prelim_pass


def main():

    lenght_of_pass = randomWordLen()
    special_char_set = ['-', '_', '.', '@']
    char_set = list(string.ascii_letters)
    psswd = randomChar(special_char_set, char_set, lenght_of_pass)
    print(''.join(psswd))


if __name__ == "__main__":
    main()
